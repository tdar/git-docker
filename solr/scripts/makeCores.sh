#!/bin/bash

if [ -f /data/search.jar ]; then
    cd /tmp
    unzip -q -o /data/search.jar 
    
    rm -Rrf /solrhome/*
    mv -f solr/* /solrhome/
fi

