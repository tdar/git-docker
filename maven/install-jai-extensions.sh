#!/bin/sh

# if $JAVA_HOME is not set, use assumed default
if [ -z ${JAVA_HOME+x} ]; then 
JAVA_HOME=/usr/lib/jvm/java-8-oracle/
fi

# expand files from tgz into right place
if [ ! -f $JAVA_HOME/COPYRIGHT-jai.txt ]; then 
  cd $JAVA_HOME
  echo "installing java image extensions after java changed into $JAVA_HOME"
  tar -xvzf /tmp/java-jai.tgz 
fi
