if [ ! -d /data/filestore ]; then
    mkdir /data/filestore
fi


if [ ! -d /data/personal-filestore ]; then
    mkdir /data/personal-filestore
fi

if [ ! -d /data/hosted-filestore ]; then
    mkdir /data/hosted-filestore
fi