#!/bin/sh

JAVA_OPTS="-Djava.awt.headless=true -XX:+UseConcMarkSweepGC -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp"
JAVA_OPTS="$JAVA_OPTS -Xms1024m -Xmx2048m  -XX:PermSize=256m -XX:MaxPermSize=1024m -server -Djava.library.path=${PATH}:/usr/lib/x86_64-linux-gnu/"
echo "HI SETENV ${JAVA_OPTS}"