#!/bin/bash

RETRIES=50

until PGPASSWORD=$PG_PASSWORD psql -h $PG_HOST -U $PG_USER -d $PG_DATABASE -c "select 1" > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
  echo "Waiting for postgres server, $((RETRIES)) remaining attempts..."
  RETRIES=$((RETRIES-=1))
  sleep 1
done

cd /liquibase
if [ -f database.jar ]; then
    echo "running liquibase"
    java -jar /var/lib/liquibase.jar --username=$PG_USER --password=$PG_PASSWORD --url=$PG_PREFIX://$PG_HOST/$PG_DATABASE --driver=$PG_CLASS --classpath=/var/lib/postgresql.jar:database.jar  --changeLogFile=liquibase/changelog.xml update
fi

/opt/tomcat/bin/catalina.sh run