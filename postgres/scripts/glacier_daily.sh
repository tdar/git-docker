#!/bin/bash

DIR=$1
TMPDIR='/tmp'
FILENAME='pg_backup.tgz'
PROPERTY_FILE="~/.s3cfg"

if [ $(id -u) -eq 0 ]
then
  PROPERTY_FILE="/root/.s3cfg"
fi


function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   # this is how BASH functions return values to their caller
   echo $PROP_VALUE
}

MAGIC_WORD=$(getProperty "gpg_passphrase")


if [ -e "$TMPDIR/$FILENAME" ]
then
  rm $TMPDIR/$FILENAME
fi

if [ -e $DIR ]
then
  find $DIR/daily $DIR/weekly -type f -mtime -1  -print0 | tar -cvzf "$TMPDIR/$FILENAME" --null --absolute-names -T - 
  gpg2 --yes --batch --passphrase="$MAGIC_WORD" --cipher-algo AES256 -c -o "$TMPDIR/$FILENAME.gpg" "$TMPDIR/$FILENAME"
  s3cmd  --no-encrypt -s  -q put "$TMPDIR/$FILENAME.gpg" "s3://tdar/postgres_daily/$FILENAME.gpg"
fi
