#!/bin/bash

# This script will create a new pg baseline backup and cull all WAL files older the specified standby window

# location of postgres data directory (comment out if this is exported from elsewhere)
#PGDATA=/usr/local/pgsql/data

# backup destinations
BASELINE_DEST=/shared/postgres/wal_archive/base
WAL_DEST=/shared/postgres/wal_archive/pg_xlog
BASEFILENAME=base.tar.$(date +%y%m%d).gz

LOG=/var/log/postgresql/wal_backup.log

# some stuff in stderr can be ignored
ERRIGNORE="skipping special file ...server"

# include 2 hours of extra walfiles to ensure we don't have a gap  of data since last snapshot
WINDOWSIZE=+2

# now create another baseline
# (fiter ignorables from stderr, send stdout to log)
#TODO: rewrite this redirection logic to be less perl-like 
pg_basebackup --pgdata=$BASELINE_DEST/temp --format=tar --gzip --xlog --username=postgres 2>&1  1>>$LOG | grep -v "$ERRIGNORE" 1>&2
mv $BASELINE_DEST/temp/base.tar.gz $BASELINE_DEST/$BASEFILENAME

# complain if no files are found in $WAL_DEST or $BASELINE_DEST
for dest in "$WAL_DEST" "$BASELINE_DEST"; do
    NUMBER_OF_FILES=`find $dest -maxdepth 1 -type f | wc -l`
    if [ "$NUMBER_OF_FILES" -eq 0 ]; then
        echo "No files found in $dest"
    fi
done
# delete waldata older than the baseline
find $WAL_DEST -mtime $WINDOWSIZE -type f -exec rm -v {} 1>>$LOG \;
find $BASELINE_DEST -mtime $WINDOWSIZE -type f -exec rm -v {} 1>>$LOG \;

