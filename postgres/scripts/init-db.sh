#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER tdar;
    ALTER USER tdar WITH PASSWORD 'tdar';
    CREATE DATABASE tdarmetadata with owner 'tdar';
    CREATE DATABASE tdardata with owner 'tdar';
    CREATE DATABASE test_tdarmetadata with owner 'tdar';
    CREATE DATABASE test_tdardata with owner 'tdar';
    CREATE DATABASE tdargis template template_postgis owner 'tdar';
    GRANT ALL PRIVILEGES ON DATABASE tdarmetadata TO tdar;
    GRANT ALL PRIVILEGES ON DATABASE tdardata TO tdar;
    GRANT ALL PRIVILEGES ON DATABASE test_tdarmetadata TO tdar;
    GRANT ALL PRIVILEGES ON DATABASE test_tdardata TO tdar;
    GRANT ALL PRIVILEGES ON DATABASE tdargis TO tdar;
EOSQL
mkdir -pv /var/lib/postgresql/backups/data/
# echo  "0 1 * * * pg_dump -U tdar tdar > /var/lib/postgresql/data/backups/\`date  +%Y-%m-%d\`.sql" > .crontab
# touch /var/log/cron.log
# /etc/init.d/cron start

# crontab .crontab

echo "/data/tdarmetadata.sql.gz"
if [ -f /data/tdarmetadata.sql.gz ]; then
    echo "gunzip: /data/tdarmetadata.sql.gz"
    gunzip /data/tdarmetadata.sql.gz
    echo "loading: /data/tdarmetadata.sql"
    psql -U tdar -f /data/tdarmetadata.sql tdarmetadata
fi


echo "/data/tdardata.sql.gz"
if [ -f /data/tdardata.sql.gz ]; then
    echo "gunzip: /data/tdardata.sql.gz"
    gunzip /data/tdardata.sql.gz
    echo "loading: /data/tdardata.sql"
    psql -U tdar -f /data/tdardata.sql tdardata
fi

echo "/data/tdargis.sql.gz"
if [ -f /data/tdargis.sql.gz ]; then
    echo "gunzip: /data/tdargis.sql.gz"
    gunzip /data/tdargis.sql.gz
    echo "loading: /data/tdargis.sql"
    psql -U tdar -f /data/tdargis.sql tdardata
fi