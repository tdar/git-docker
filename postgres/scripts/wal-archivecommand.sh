#!/bin/bash

# this script gets executed by pgsql  via the "wal_commmand" setting in postgrescql.conf.  Notes:
#   * $1 is the relative path to the wal file (e.g. "pg_xlog/12345678")
#   * $2 is the filename only (e.g "12345678")
#   * failure must exit w/ non-zero code, otherwise pgsql will think the operation was successful
#   * this script must not allow files to be overwritten

# LOG=/var/log/postgres/archive_command.log
# DEST=/shared/postgres/backups/wal_archive/pg_xlog

LOG=/var/log/postgresql/archive_command.log
DEST=/shared/postgres/wal_archive/pg_xlog

SRCFILE=$1
FILENAME=$2
DESTFILE=$DEST/$FILENAME.gz

if [ ! -e "$DEST" ]; then
        mkdir -p $DEST
fi

echo "writing:$(date)   pwd:$PWD    args:$0  " >> $LOG

# throw error if target file exists
if [ -e "$DESTFILE" ]
    then
        echo ERROR - file already exists: $DESTFILE >&2
        exit 1
fi

#gzip the file to $DESTFILE.  throw error if operation fails
gzip  $SRCFILE -c > $DESTFILE
retval=$?

if [ $retval -ne 0 ] 
    then
        echo ERROR - operation failed: "\"gzip $SRCFILE -c > $DESTFILE\""
        exit 1
fi

#output file+size to log
echo $(ls -hal $DESTFILE) >> $LOG

exit 0

